import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TabsPage } from '../pages/tabs/tabs';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Media, MediaObject } from '@ionic-native/media';
import { AlertController } from 'ionic-angular';

//import { PopupPage } from '../pages/popup/popup';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = TabsPage;
  file: MediaObject;
  constructor(platform: Platform, statusBar: StatusBar,
    splashScreen: SplashScreen,
    localNotifications: LocalNotifications,
    media: Media,
    alertCtrl: AlertController
  ) {
    platform.ready().then(() => {
      
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      setInterval(function () {
        const alarmData = { 'hour': '', 'minute': '', 'track': '' };
        var currentDate = new Date();
        var days = ["sun", "mon", "tue", "wed", "thu", "fri", "sat"];
        this.checkAlarmTime = JSON.parse(localStorage.getItem('alarmListArr'));
        if (this.checkAlarmTime != null) {
          for (let i = 0; i < this.checkAlarmTime.length; i++) {
            if (this.checkAlarmTime[i].hours == currentDate.getHours()) {
              console.log("correct hours");
              if (this.checkAlarmTime[i].minutes == currentDate.getMinutes()) {
                console.log("correct minutes");
                if (this.checkAlarmTime[i].days == days[currentDate.getDay()]) {
                  console.log("correct days");
                  if (this.checkAlarmTime[i].toggle == "true") {
                    console.log("correct toggle");
                    this.alarmData = {
                      hour: currentDate.getHours(),
                      minute: currentDate.getMinutes(),
                      track: this.checkAlarmTime[i].track
                    }
                    localStorage.setItem('trackSelected', JSON.stringify(this.alarmData.track));
                    localNotifications.schedule({
                      id: 1,
                      title: 'Alarm',
                      text: 'Alarm is on',
                      data: { mydata: 'My hidden files' }
                    });
                    platform.ready().then((rdy) => {
                      this.file = media.create('../assets/alarm/alarm1.mp3');
                      //file.onSuccess.subscribe(() => console.log('Action is successful'));
                      this.file.onError.subscribe(error => console.log('Error!', error));
                      this.file.play();
                      this.presentConfirmationDialog('Do you want to Dissmiss alarm');
                      localNotifications.on('click', (notification, state) => {
                        // let snooze = modal.create(PopupPage, { data: this.alarmData }, { cssClass: 'contact-model' });
                        // snooze.present();
                      });
                    });
                  }
                }
              }
            }
          }
        }
      }, 20000);

    });
  }

  presentConfirmationDialog(msg: string) {
    const alert = this.alertCtrl.create({
      title: 'Are you sure',
      message: msg,
      buttons: [
        {
          text: 'Dissmiss Alarm',
          role: 'Yes',
          handler: () => {
            this.file.stop();
            console.log('Yes clicked');
          }
        },
        {
          text: 'No',
          handler: () => {
            console.log('you canceled this action...');
          }
        }
      ]
    });
    alert.present();
  }
}

