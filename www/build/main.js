webpackJsonp([3],{

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlarmListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AlarmListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AlarmListPage = (function () {
    function AlarmListPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.getAllAlarmListArr = [];
    }
    AlarmListPage.prototype.ionViewWillEnter = function () {
        this.getAllAlarmListArr = JSON.parse(localStorage.getItem('alarmListArr'));
        console.log(this.getAllAlarmListArr);
    };
    AlarmListPage.prototype.trackChange = function (value, index) {
        if (value == true) {
            this.getAllAlarmListArr[index].toggle = true;
        }
        if (value == false) {
            this.getAllAlarmListArr[index].toggle = false;
        }
        localStorage.setItem('alarmListArr', JSON.stringify(this.getAllAlarmListArr));
    };
    AlarmListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-alarm-list',template:/*ion-inline-start:"E:\Ionic\alarm-clock\src\pages\alarm-list\alarm-list.html"*/'<!--\n  Generated template for the AlarmListPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header style="border: 2px solid #eef435;">\n\n  <ion-navbar color="dark">\n    <ion-title style="text-align:center;">Alarm List</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding style="background:black;border: 2px solid #eef435;">\n  <ion-row *ngFor="let getAllAlarmList of getAllAlarmListArr; let i = index; "  class="row-cls" style="border-bottom: 2px solid #000000;">\n      <div class="hours-cls">\n       <div *ngIf="getAllAlarmList.hours == 0" style="float:left;">0</div><div style="float:left;">{{getAllAlarmList.hours}}:</div><div *ngIf="getAllAlarmList.minutes == 0" style="float:left;">0</div><div style="float:left;">{{getAllAlarmList.minutes}}</div>\n      </div>\n      <div class="days-cls">\n        {{getAllAlarmList.days}}\n      </div>\n      <div class="toggle-cls">\n        <ion-toggle color="toggleColor"  [(ngModel)]="getAllAlarmList.toggle" (ngModelChange)="trackChange($event,i)"></ion-toggle>\n        \n      </div>\n  </ion-row>\n</ion-content>\n'/*ion-inline-end:"E:\Ionic\alarm-clock\src\pages\alarm-list\alarm-list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], AlarmListPage);
    return AlarmListPage;
}());

//# sourceMappingURL=alarm-list.js.map

/***/ }),

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__alarm_list_alarm_list__ = __webpack_require__(101);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TabsPage = (function () {
    function TabsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.homePage = __WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */];
        this.alarmListPage = __WEBPACK_IMPORTED_MODULE_3__alarm_list_alarm_list__["a" /* AlarmListPage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tabs',template:/*ion-inline-start:"E:\Ionic\alarm-clock\src\pages\tabs\tabs.html"*/'<ion-tabs color="dark">\n  <ion-tab [root]="homePage" tabTitle="Add Alarm"></ion-tab>\n  <ion-tab [root]="alarmListPage" tabTitle="Alarms"></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"E:\Ionic\alarm-clock\src\pages\tabs\tabs.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 113:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 113;

/***/ }),

/***/ 154:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/alarm-list/alarm-list.module": [
		276,
		2
	],
	"../pages/popup/popup.module": [
		278,
		1
	],
	"../pages/tabs/tabs.module": [
		277,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 154;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 155:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_local_notifications__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_media__ = __webpack_require__(79);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HomePage = (function () {
    function HomePage(navCtrl, toastCtrl, modal, localNotifications, platform, media) {
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.modal = modal;
        this.localNotifications = localNotifications;
        this.platform = platform;
        this.media = media;
        this.checkAlarmTime = [];
        this.alarmListArr = [];
       platform.ready().then(function (rdy) { 
        setInterval(function () {
            var alarmData = { 'hour': '', 'minute': '', 'track': '' };
            var currentDate = new Date();
            var days = ["sun", "mon", "tue", "wed", "thu", "fri", "sat"];
            this.checkAlarmTime = JSON.parse(localStorage.getItem('alarmListArr'));
            //alert(this.checkAlarmTime);
            //alert(days[currentDate.getDay()]+ "----" +currentDate.getHours() + "---"+ currentDate.getMinutes())
            if (this.checkAlarmTime != null) {
                for (var i = 0; i < this.checkAlarmTime.length; i++) {
                    if (this.checkAlarmTime[i].hours == currentDate.getHours()) {
                        console.log("correct hours");
                        if (this.checkAlarmTime[i].minutes == currentDate.getMinutes()) {
                            console.log("correct minutes");
                            if (this.checkAlarmTime[i].days == days[currentDate.getDay()]) {
                                console.log("correct days");
                                if (this.checkAlarmTime[i].toggle == "true") {
                                    console.log("correct toggle");
                                    this.alarmData = {
                                        hour: currentDate.getHours(),
                                        minute: currentDate.getMinutes(),
                                        track: this.checkAlarmTime[i].track
                                    };
                                    localStorage.setItem('trackSelected', JSON.stringify(this.alarmData.track));
                                    localNotifications.schedule({
                                        id: 1,
                                        title: 'Alarm',
                                        text: 'Alarm is on',
                                        data: { mydata: 'My hidden files' }
                                    });
                                    	
                                        var file = media.create('file:///android_asset/www/assets/alarm/'+this.checkAlarmTime[i].track);
                                        //file.onSuccess.subscribe(() => console.log('Action is successful'));
                                        file.onError.subscribe(function (error) { return console.log('Error!', error); });

                                        file.play();
                                        localNotifications.on('click', function (notification, state) {
                                            // let snooze = modal.create(PopupPage, { data: this.alarmData }, { cssClass: 'contact-model' });
                                            // snooze.present();
                                        });
                                    
                                }
                            }
                        }
                    }
                }
            }
        }, 10000);
        });                                
    }
    HomePage.prototype.ionViewDidLoad = function () {
        console.log('Hello World');
    };
    HomePage.prototype.presentToastHour = function () {
        var toast = this.toastCtrl.create({
            message: 'Please enter hour to set alarm',
            duration: 3000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    HomePage.prototype.presentToastMinutes = function () {
        var toast = this.toastCtrl.create({
            message: 'Please enter minute to set alarm',
            duration: 3000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    HomePage.prototype.presentToastDays = function () {
        var toast = this.toastCtrl.create({
            message: 'Please enter day to set alarm',
            duration: 3000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    HomePage.prototype.presentToastTracks = function () {
        var toast = this.toastCtrl.create({
            message: 'Please enter track to set alarm',
            duration: 3000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    HomePage.prototype.SuccessAlarmAdded = function () {
        var toast = this.toastCtrl.create({
            message: 'New Alarm is set',
            duration: 3000,
            position: 'bottom'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    HomePage.prototype.hoursChange = function (hours) {
        this.hours = hours;
    };
    HomePage.prototype.minutesChange = function (minutes) {
        this.minutes = minutes;
    };
    HomePage.prototype.daysChange = function (days) {
        this.days = days;
    };
    HomePage.prototype.trackChange = function (tracks) {
        this.tracks = tracks;
    };
    HomePage.prototype.addAlarm = function () {
        if (this.hours == undefined) {
            this.presentToastHour();
        }
        else if (this.minutes == undefined) {
            this.presentToastMinutes();
        }
        else if (this.days == undefined) {
            this.presentToastDays();
        }
        else if (this.tracks == undefined) {
            this.presentToastTracks();
        }
        else {
            this.alarmListArr.push({
                'hours': this.hours,
                'minutes': this.minutes,
                'days': this.days,
                'track': this.tracks,
                'toggle': 'true'
            });
            localStorage.setItem('alarmListArr', JSON.stringify(this.alarmListArr));
            this.hours = null;
            this.minutes = null;
            this.days = null;
            this.tracks = null;
            this.SuccessAlarmAdded();
        }
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"E:\Ionic\alarm-clock\src\pages\home\home.html"*/'<ion-header style="border:2px solid #eef435;">\n  <ion-navbar color="dark">\n    <ion-title style="text-align:center;">\n      Add Alarm\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding style="background-color:black; border:2px solid #eef435;">\n  <ion-item style="background-color:black;color:#ea00c3;">\n    <ion-label>Hours</ion-label>\n    <ion-select [(ngModel)]="hours" (ngModelChange)="hoursChange($event)" required>\n      <ion-option value="01">01</ion-option>\n      <ion-option value="02">02</ion-option>\n      <ion-option value="03">03</ion-option>\n      <ion-option value="04">04</ion-option>\n      <ion-option value="05">05</ion-option>\n      <ion-option value="06">06</ion-option>\n      <ion-option value="07">07</ion-option>\n      <ion-option value="08">08</ion-option>\n      <ion-option value="09">09</ion-option>\n      <ion-option value="10">10</ion-option>\n      <ion-option value="11">11</ion-option>\n      <ion-option value="12">12</ion-option>\n      <ion-option value="13">13</ion-option>\n      <ion-option value="14">14</ion-option>\n      <ion-option value="15">15</ion-option>\n      <ion-option value="16">16</ion-option>\n      <ion-option value="17">17</ion-option>\n      <ion-option value="18">18</ion-option>\n      <ion-option value="19">19</ion-option>\n      <ion-option value="20">20</ion-option>\n      <ion-option value="21">21</ion-option>\n      <ion-option value="22">22</ion-option>\n      <ion-option value="23">23</ion-option>\n      <ion-option value="0">00</ion-option>\n    </ion-select>\n  </ion-item>\n  <ion-item style="background-color:black;color:#ea00c3;">\n      <ion-label>Minutes</ion-label>\n      <ion-select [(ngModel)]="minutes" (ngModelChange)="minutesChange($event)">\n          <ion-option value="01">01</ion-option>\n          <ion-option value="02">02</ion-option>\n          <ion-option value="03">03</ion-option>\n          <ion-option value="04">04</ion-option>\n          <ion-option value="05">05</ion-option>\n          <ion-option value="06">06</ion-option>\n          <ion-option value="07">07</ion-option>\n          <ion-option value="08">08</ion-option>\n          <ion-option value="09">09</ion-option>\n          <ion-option value="10">10</ion-option>\n          <ion-option value="11">11</ion-option>\n          <ion-option value="12">12</ion-option>\n          <ion-option value="13">13</ion-option>\n          <ion-option value="14">14</ion-option>\n          <ion-option value="15">15</ion-option>\n          <ion-option value="16">16</ion-option>\n          <ion-option value="17">17</ion-option>\n          <ion-option value="18">18</ion-option>\n          <ion-option value="19">19</ion-option>\n          <ion-option value="20">20</ion-option>\n          <ion-option value="21">21</ion-option>\n          <ion-option value="22">22</ion-option>\n          <ion-option value="23">23</ion-option>\n          <ion-option value="24">24</ion-option>\n          <ion-option value="25">25</ion-option>\n          <ion-option value="26">26</ion-option>\n          <ion-option value="27">27</ion-option>\n          <ion-option value="28">28</ion-option>\n          <ion-option value="29">29</ion-option>\n          <ion-option value="30">30</ion-option>\n          <ion-option value="31">31</ion-option>\n          <ion-option value="32">32</ion-option>\n          <ion-option value="33">33</ion-option>\n          <ion-option value="34">34</ion-option>\n          <ion-option value="35">35</ion-option>\n          <ion-option value="36">36</ion-option>\n          <ion-option value="37">37</ion-option>\n          <ion-option value="38">38</ion-option>\n          <ion-option value="39">39</ion-option>\n          <ion-option value="40">40</ion-option>\n          <ion-option value="41">41</ion-option>\n          <ion-option value="42">42</ion-option>\n          <ion-option value="43">43</ion-option>\n          <ion-option value="44">44</ion-option>\n          <ion-option value="45">45</ion-option>\n          <ion-option value="46">46</ion-option>\n          <ion-option value="47">47</ion-option>\n          <ion-option value="48">48</ion-option>\n          <ion-option value="49">49</ion-option>\n          <ion-option value="50">50</ion-option>\n          <ion-option value="51">51</ion-option>\n          <ion-option value="52">52</ion-option>\n          <ion-option value="53">53</ion-option>\n          <ion-option value="54">54</ion-option>\n          <ion-option value="55">55</ion-option>\n          <ion-option value="56">56</ion-option>\n          <ion-option value="57">57</ion-option>\n          <ion-option value="58">58</ion-option>\n          <ion-option value="59">59</ion-option>\n          <ion-option value="0">00</ion-option>\n      </ion-select>\n    </ion-item>\n    <ion-item style="background-color:black;color:#ea00c3;">\n        <ion-label>Days</ion-label>\n        <ion-select [(ngModel)]="days" (ngModelChange)="daysChange($event)">\n          <ion-option value="mon">Monday</ion-option>\n          <ion-option value="tue">Tuesday</ion-option>\n          <ion-option value="wed">Wednesday</ion-option>\n          <ion-option value="thu">Thursday</ion-option>\n          <ion-option value="fri">Friday</ion-option>\n          <ion-option value="sat">Saturday</ion-option>\n          <ion-option value="sun">Sunday</ion-option>\n        </ion-select>\n      </ion-item>\n      <ion-item style="background-color:black;color:#ea00c3;">\n          <ion-label>Track</ion-label>\n          <ion-select [(ngModel)]="tracks" (ngModelChange)="trackChange($event)">\n            <ion-option value="track1.mp3">Hey Fucking Santa Clause Where\'s My Fucking Bike</ion-option>\n            <ion-option value="track2.mp3">I Love My Women Like I Like My Chicken</ion-option>\n            <ion-option value="track3.mp3">Who Ate All The Pies</ion-option>\n          </ion-select>\n        </ion-item>\n          <button ion-button full (click)=\'addAlarm();\' style="top:15%;color:#ea00c3;background-color:#eef435;">Create Alarm</button>\n</ion-content>\n'/*ion-inline-end:"E:\Ionic\alarm-clock\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* ModalController */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_local_notifications__["a" /* LocalNotifications */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Platform */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_media__["a" /* Media */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 199:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_media__ = __webpack_require__(79);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the PopupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PopupPage = (function () {
    function PopupPage(navParams, view, media) {
        this.navParams = navParams;
        this.view = view;
        this.media = media;
    }
    PopupPage.prototype.ionViewWillLoad = function () {
        var alarmData = this.navParams.get('data');
        localStorage.setItem('alarmDataObject', JSON.stringify(alarmData));
        if (alarmData.track == 'track1.mp3') {
            var file = this.media.create('/alarm1.mp3');
            file.play();
        }
        else if (alarmData.track == 'track2.mp3') {
            var file = this.media.create('/alarm2.mp3');
            file.play();
        }
        else {
            var file = this.media.create('/alarm3.mp3');
            file.play();
        }
        this.alarmDataFullObject = JSON.parse(localStorage.getItem('alarmDataObject'));
    };
    PopupPage.prototype.dismiss = function () {
        this.view.dismiss();
        if (JSON.parse(localStorage.getItem('trackSelected')) == 'track1.mp3') {
            var file = this.media.create('/alarm1.mp3');
            file.stop();
            file.release();
        }
        else if (JSON.parse(localStorage.getItem('trackSelected')) == 'track2.mp3') {
            var file = this.media.create('/alarm2.mp3');
            file.stop();
            file.release();
        }
        else {
            var file = this.media.create('/alarm3.mp3');
            file.stop();
            file.release();
        }
    };
    PopupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-popup',template:/*ion-inline-start:"E:\Ionic\alarm-clock\src\pages\popup\popup.html"*/'<!--\n  Generated template for the PopupPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header style="position:absolute;top:25%;">\n\n  <ion-navbar>\n    <ion-title style="text-align:center;">Alarm</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content class="content-cls" padding style="background-color:black;border: 2px solid #eef435;position:absolute;top:25%;">\n  <div class="wrapper-div-cls">\n    <div class="time-cls">\n      <div *ngIf="alarmDataFullObject.hour == 0" style="float:left;">0</div><div style="float:left;">{{alarmDataFullObject.hour}}:</div><div *ngIf="alarmDataFullObject.minute == 0" style="float:left;">0</div><div style="float:left;">{{alarmDataFullObject.minute}}</div>\n    </div>\n  </div>\n  <button ion-button full (click)=\'dismiss();\' style="color:#ea00c3;background-color:#eef435;top:20%;">Dismiss</button>\n</ion-content>\n'/*ion-inline-end:"E:\Ionic\alarm-clock\src\pages\popup\popup.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_media__["a" /* Media */]])
    ], PopupPage);
    return PopupPage;
}());

//# sourceMappingURL=popup.js.map

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(224);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 224:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(275);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_alarm_list_alarm_list__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_tabs_tabs__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_popup_popup__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_media__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_local_notifications__ = __webpack_require__(156);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_alarm_list_alarm_list__["a" /* AlarmListPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_popup_popup__["a" /* PopupPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/alarm-list/alarm-list.module#AlarmListPageModule', name: 'AlarmListPage', segment: 'alarm-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tabs/tabs.module#TabsPageModule', name: 'TabsPage', segment: 'tabs', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/popup/popup.module#PopupPageModule', name: 'PopupPage', segment: 'popup', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_alarm_list_alarm_list__["a" /* AlarmListPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_popup_popup__["a" /* PopupPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_media__["a" /* Media */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_local_notifications__["a" /* LocalNotifications */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 275:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__ = __webpack_require__(102);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





//import { PopupPage } from '../pages/popup/popup';
var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"E:\Ionic\alarm-clock\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"E:\Ionic\alarm-clock\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ })

},[200]);
//# sourceMappingURL=main.js.map