import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController} from 'ionic-angular';
import { Media, MediaObject } from '@ionic-native/media';

/**
 * Generated class for the PopupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-popup',
  templateUrl: 'popup.html',
})
export class PopupPage {

  constructor( private navParams: NavParams, private view: ViewController, private media: Media) {
  }
  public alarmDataFullObject;
  ionViewWillLoad() {
    const alarmData = this.navParams.get('data');
    localStorage.setItem('alarmDataObject',JSON.stringify(alarmData));
    if(alarmData.track == 'track1.mp3'){
      const file: MediaObject = this.media.create('../assets/alarm/alarm1.mp3');
      file.play();
    }
    else if(alarmData.track == 'track2.mp3'){
      const file: MediaObject = this.media.create('../assets/alarm/alarm2.mp3');
      file.play();
    }
    else{
      const file: MediaObject = this.media.create('../assets/alarm/alarm3.mp3');
      file.play();
    }
    this.alarmDataFullObject = JSON.parse(localStorage.getItem('alarmDataObject'));
  }
  dismiss(){
    this.view.dismiss();
    if(JSON.parse(localStorage.getItem('trackSelected')) == 'track1.mp3'){
      const file: MediaObject = this.media.create('../assets/alarm/alarm1.mp3');
      file.stop();
      file.release();
    }
    else if(JSON.parse(localStorage.getItem('trackSelected')) == 'track2.mp3'){
      const file: MediaObject = this.media.create('../assets/alarm/alarm2.mp3');
      file.stop();
      file.release();
    }
    else{
      const file: MediaObject = this.media.create('../assets/alarm/alarm3.mp3');
      file.stop();
      file.release();
    }
    
  }

}
