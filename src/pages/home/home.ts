import { Component } from '@angular/core';
import { NavController, ModalController, Platform } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { PopupPage } from '../popup/popup';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Media, MediaObject } from '@ionic-native/media';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  constructor(public navCtrl: NavController,
    private toastCtrl: ToastController,
    public modal: ModalController,
    public localNotifications: LocalNotifications,
    public platform: Platform,
    private media: Media) {
    
  }
  public checkAlarmTime = [];
  public hours;
  public minutes;
  public days;
  public tracks;
  public alarmListArr = [];
  public toggle;

  ionViewDidLoad() {
    console.log('Hello World');
  }
  presentToastHour() {
    let toast = this.toastCtrl.create({
      message: 'Please enter hour to set alarm',
      duration: 3000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
  presentToastMinutes() {
    let toast = this.toastCtrl.create({
      message: 'Please enter minute to set alarm',
      duration: 3000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
  presentToastDays() {
    let toast = this.toastCtrl.create({
      message: 'Please enter day to set alarm',
      duration: 3000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
  presentToastTracks() {
    let toast = this.toastCtrl.create({
      message: 'Please enter track to set alarm',
      duration: 3000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
  SuccessAlarmAdded() {
    let toast = this.toastCtrl.create({
      message: 'New Alarm is set',
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
  hoursChange(hours) {
    this.hours = hours;
  }
  minutesChange(minutes) {
    this.minutes = minutes;
  }
  daysChange(days) {
    this.days = days;

  }
  trackChange(tracks) {
    this.tracks = tracks;
  }
  addAlarm() {
    if (this.hours == undefined) {
      this.presentToastHour();
    }
    else if (this.minutes == undefined) {
      this.presentToastMinutes();
    }
    else if (this.days == undefined) {
      this.presentToastDays();
    }
    else if (this.tracks == undefined) {
      this.presentToastTracks();
    }
    else {
      this.alarmListArr.push({
        'hours': this.hours,
        'minutes': this.minutes,
        'days': this.days,
        'track': this.tracks,
        'toggle': 'true'
      });
      localStorage.setItem('alarmListArr', JSON.stringify(this.alarmListArr));
      this.hours = null;
      this.minutes = null;
      this.days = null;
      this.tracks = null;
      this.SuccessAlarmAdded();
    }
  }
}
