import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AlarmListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-alarm-list',
  templateUrl: 'alarm-list.html',
})
export class AlarmListPage {
  public getAllAlarmListArr = [];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  
  ionViewWillEnter(){

    this.getAllAlarmListArr = JSON.parse(localStorage.getItem('alarmListArr'));
    console.log(this.getAllAlarmListArr);
  }
  trackChange(value,index){
    if(value == true){
      this.getAllAlarmListArr[index].toggle = true;
    }
    if(value == false){
      this.getAllAlarmListArr[index].toggle = false;
    }
    localStorage.setItem('alarmListArr',JSON.stringify(this.getAllAlarmListArr));
  }
  
}
